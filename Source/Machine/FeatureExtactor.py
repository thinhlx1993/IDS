from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
import numpy as np

class EmptyFitMixin:
    def fit(self, x, y=None):
        return self

class TextExtractor(BaseEstimator, TransformerMixin, EmptyFitMixin):
    def __init__(self, text_cols=[ 'method', 'url', 'protocol', 'userAgent', 'pragma',
       'cacheControl', 'accept', 'acceptEncoding', 'acceptCharset',
       'acceptLanguage', 'host', 'connection', 'contentLength', 'contentType'
    ]):
        self.text_cols = text_cols
    def transform(self, data):
        def join(items):
            return ' '.join([str(item) for item in items])
            #return ' '.join([str(lxml.html.tostring(item)) for item in items])

        texts = data[self.text_cols].apply(join, axis=1)
        return texts
class ItemSelector(BaseEstimator, TransformerMixin, EmptyFitMixin):
    def __init__(self, key):
        self.key = key
    def transform(self, df):
        values = np.array(df[self.key])
        return values.reshape(values.shape[0], 1)
class LengthExtractor(BaseEstimator, TransformerMixin, EmptyFitMixin):
    def transform(self, data):
        values = np.array([len(d) for d in data])
        return values.reshape(values.shape[0], 1)
