from sklearn.pipeline import Pipeline
from sklearn.pipeline import FeatureUnion
import Machine.FeatureExtactor
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd

TextExtractor = Machine.FeatureExtactor.TextExtractor

def feature_union():
    df = pd.read_csv('//home/thinh/Desktop/csicchuan.csv',sep=',',header=0,low_memory=False)
    feature_union_both = FeatureUnion(
        transformer_list=[
            # ('index', Pipeline([
            #     ('index', TextExtractor(text_cols=['index'])),
            #     ('tfidf', TfidfVectorizer()),
            # ])),
            ('method', Pipeline([
                ('method', TextExtractor(text_cols=['method'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('url', Pipeline([
                ('url', TextExtractor(text_cols=['url'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('protocol', Pipeline([
                ('protocol', TextExtractor(text_cols=['protocol'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('userAgent', Pipeline([
                ('userAgent', TextExtractor(text_cols=['userAgent'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('pragma', Pipeline([
                ('pragma', TextExtractor(text_cols=['pragma'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('cacheControl', Pipeline([
                ('cacheControl', TextExtractor(text_cols=['cacheControl'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('accept', Pipeline([
                ('accept', TextExtractor(text_cols=['accept'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('acceptEncoding', Pipeline([
                ('acceptEncoding', TextExtractor(text_cols=['acceptEncoding'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('acceptCharset', Pipeline([
                ('acceptCharset', TextExtractor(text_cols=['acceptCharset'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('acceptLanguage', Pipeline([
                ('acceptLanguage', TextExtractor(text_cols=['acceptLanguage'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('host', Pipeline([
                ('host', TextExtractor(text_cols=['host'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('connection', Pipeline([
                ('connection', TextExtractor(text_cols=['connection'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('contentLength', Pipeline([
                ('contentLength', TextExtractor(text_cols=['contentLength'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            ('contentType', Pipeline([
                ('contentType', TextExtractor(text_cols=['contentType'])),
                ('tfidf', TfidfVectorizer()),
            ])),
            # ('cookie', Pipeline([
            #     ('cookie', TextExtractor(text_cols=['cookie'])),
            #     ('tfidf', TfidfVectorizer()),
            # ])),
            # ('payload', Pipeline([
            #     ('payload', TextExtractor(text_cols=['payload'])),
            #     ('tfidf', TfidfVectorizer()),
            # ])),
        ],
    )
    X_both = feature_union_both.fit_transform(df)
    return X_both,df