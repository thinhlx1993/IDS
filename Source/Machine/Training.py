from pygments.modeline import modeline_re
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.ensemble import voting_classifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
import pandas as pd
import joblib
from Machine import FeatureUnion


def training():
    X_both,df = FeatureUnion.feature_union()
    X_both_train, X_both_test, y_both_train, y_both_test = train_test_split(X_both, df['label'], test_size=0.33)
    model_randomforest = RandomForestClassifier(n_jobs=10)
    model_randomforest.fit(X_both_train, y_both_train)
    y_pred_randomforest = model_randomforest.predict(X_both_test)
    print(classification_report(y_both_test, y_pred_randomforest))
    bdt_real = AdaBoostClassifier(DecisionTreeClassifier(max_depth=2),n_estimators=1,learning_rate=1)
    bdt_discrete = AdaBoostClassifier(DecisionTreeClassifier(max_depth=600),n_estimators=1,learning_rate=1.5,algorithm="SAMME")
    bdt_real.fit(X_both_train,y_both_train)
    bdt_discrete.fit(X_both_train,y_both_train)
    y_pred_bdt_real  = bdt_real.predict(X_both_test)
    print(classification_report(y_both_test,y_pred_bdt_real))
    y_pred_bdt_discrete = bdt_discrete.predict(X_both_test)
    print(classification_report(y_both_test,y_pred_bdt_discrete))
    print('end')

    return
training()