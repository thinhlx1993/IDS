import os
import sys
from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from packet import packetdata
import pyshark
import pandas as pd
supported_protocols = {}
f = open('//home/thinh/Documents/capture', 'w')
# Get supported application protocols
def get_protocols():
    global supported_protocols
    fp = None
    if os.path.isfile('./protocols.list'):
        fp = open('./protocols.list')
    protocols = fp.readlines()
    for protocol in protocols:
        protocol = protocol.strip()
        supported_protocols[protocol] = 1

# Get application level protocol
def get_highest_protocol(packet):
    global supported_protocols
    if not supported_protocols:
        get_protocols()
    for layer in reversed(packet.layers):
        if layer.layer_name in supported_protocols:
            return layer.layer_name
    return 'wtf'

# Get the protocol layer fields
def get_layer_fields(layer):
    layer_fields = {}
    for field_name in layer.field_names:
        if len(field_name) > 0:
            layer_fields[field_name] = getattr(layer, field_name)
    return layer_fields

# Returns a dictionary containing the packet layer data
def get_layers(packet):
    n = len(packet.layers)
    # highest_protocol = get_highest_protocol(packet)
    layers = {}

    # Link layer
    layers[packet.layers[0].layer_name] = get_layer_fields(packet.layers[0])
    layer_above_transport = 0

    # Get the rest of the layers
    for i in range(1,n):
        layer = packet.layers[i]

        # Network layer - ARP
        if layer.layer_name == 'arp':
            layers[layer.layer_name] = get_layer_fields(layer)
            return  layers

        # Network layer - IP or IPv6
        elif layer.layer_name == 'ip' or layer.layer_name == 'ipv6':
            layers[layer.layer_name] = get_layer_fields(layer)

        # Transport layer - TCP, UDP, ICMP, IGMP, IDMP, or ESP
        elif layer.layer_name == 'tcp' or layer.layer_name == 'udp' or layer.layer_name == 'icmp' or layer.layer_name == 'igmp' or layer.layer_name == 'idmp' or layer.layer_name == 'esp':
            layers[layer.layer_name] = get_layer_fields(layer)
            # if highest_protocol == 'tcp' or highest_protocol == 'udp' or highest_protocol == 'icmp' or highest_protocol == 'esp':
            return  layers
            layer_above_transport = i+1
            break

        # Additional transport layer data
        else:
            layers[layer.layer_name] = get_layer_fields(layer)
            layers[packet.layers[i].layer_name]['envelope'] = packet.layers[i-1].layer_name

    for j in range(layer_above_transport,n):
        layer = packet.layers[j]

        # Application layer
        if layer.layer_name == highest_protocol:
            layers[layer.layer_name] = get_layer_fields(layer)

        # Additional application layer data
        else:
            layers[layer.layer_name] = get_layer_fields(layer)
            layers[layer.layer_name]['envelope'] = packet.layers[j-1].layer_name

    return layers
#getpacket
def get_packet(packet):
    pc = packetdata
    pc._index = pc._method = pc._url = pc._protocol = pc._useragent = pc._pragma = pc._cachecontrol = pc._accept = pc._acceptencoding = pc._acceptcharset = pc._acceptlanguage = pc._host = pc._connection = pc._contentlengt = pc._contenttype = pc._cookie = pc._payload =pc._dst_host=pc._dstport=pc._src_host=pc._dstport=pc._protocol_transport =pc._srcport= 'null'
    n = len(packet.layers)
    layer_fields = {}
    layer_fields1 = {}
    layer_fields2 = {}
    if n == 4:
        layerip = packet.layers[n - 3]
        layertcp = packet.layers[n - 2]
        layer = packet.layers[n-1]
        if layer.layer_name == 'http':
            for field_name in layer.field_names:
                #print(field_name)
                if len(field_name) > 0:
                    if field_name == 'request_version':
                        pc._contentlengt = packet.length
                        # print('Content Length',pc._contentlengt)
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._protocol = layer_fields[field_name]
                        # print('Protocol: ',pc._protocol)
                    if field_name == 'request_method':
                        layer_fields[field_name] = getattr(layer,field_name)
                        pc._method = layer_fields[field_name]
                        # print('request_method: ',pc._method)
                    if field_name == 'location':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._url = layer_fields[field_name]
                        # print('URL: ',pc._url)
                    if field_name == 'accept':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._accept = layer_fields[field_name]
                        # print('accept: ',pc._accept)
                    if field_name == 'user_agent':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._useragent = layer_fields[field_name]
                        # print('user_agent: ',pc._useragent)
                    if field_name == 'accept_language':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._acceptlanguage = layer_fields[field_name]
                        # print('accept_language: ',pc._acceptlanguage)
                    if field_name == 'accept_encoding':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._acceptencoding = layer_fields[field_name]
                        # print('accept_encoding: ',pc._acceptencoding)
                    if field_name == 'accept_charset':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._acceptcharset = layer_fields[field_name]
                        # print('Accept-Charset: ',pc._acceptcharset)
                    if field_name == 'cache_control':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._cachecontrol = layer_fields[field_name]
                        # print('cache_control: ',pc._cachecontrol)
                    if field_name == 'pragma':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._pragma = layer_fields[field_name]
                        # print('pragma: ',pc._pragma)
                    if field_name == 'host':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._host = layer_fields[field_name]
                        # print('host: ',pc._host)
                    if field_name == 'connection':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._connection = layer_fields[field_name]
                        # print('connection: ',pc._connection)
                    if field_name == 'content_type':
                        layer_fields[field_name] = getattr(layer, field_name)
                        pc._contenttype = layer_fields[field_name]
                        # print('Content-Type: ',pc._contenttype)
                    # if field_name == 'cookie':
                    #     layer_fields[field_name] = getattr(layer, field_name)
                    #     pc._cookie = layer_fields[field_name]
                    #     print('cookie: ',pc._cookie)

                    # if field_name == 'payload':
                    #     layer_fields[field_name] = getattr(layer, field_name)
                    #     pc._payload = layer_fields[field_name]
                    #     print('Payload: ',pc._payload)
            for field_name in layerip.field_names:
                if len(field_name) > 0:
                    if field_name == 'src_host':
                        layer_fields1[field_name] = getattr(layerip, field_name)
                        pc._src_host = layer_fields1[field_name]
                        # print(pc._src_host )
                    if field_name == 'dst_host':
                        layer_fields1[field_name] = getattr(layerip, field_name)
                        pc._dst_host = layer_fields1[field_name]
                        # print(pc._dst_host )
            for field_name in layertcp.field_names:
                if len(field_name) > 0:
                    if field_name == 'srcport':
                        layer_fields2[field_name] = getattr(layertcp, field_name)
                        pc._srcport = layer_fields2[field_name]
                        # print(pc._srcport )
                    if field_name == 'dstport':
                        layer_fields2[field_name] = getattr(layertcp, field_name)
                        pc._dstport = layer_fields2[field_name]
                        # print(pc._dstport )
            pc._protocol_transport = packet.layers[n-1].layer_name
            return pc
    if n == 3:
        layerip = packet.layers[n - 2]
        layertcp = packet.layers[n -1]
        for field_name in layerip.field_names:
            if len(field_name) > 0:
                if field_name == 'src_host':
                    layer_fields1[field_name] = getattr(layerip, field_name)
                    pc._src_host = layer_fields1[field_name]
                    # print(pc._src_host )
                if field_name == 'dst_host':
                    layer_fields1[field_name] = getattr(layerip, field_name)
                    pc._dst_host = layer_fields1[field_name]
                    # print(pc._dst_host )
        for field_name in layertcp.field_names:
            if len(field_name) > 0:
                if field_name == 'srcport':
                    layer_fields2[field_name] = getattr(layertcp, field_name)
                    pc._srcport = layer_fields2[field_name]
                    # print(pc._srcport )
                if field_name == 'dstport':
                    layer_fields2[field_name] = getattr(layertcp, field_name)
                    pc._dstport = layer_fields2[field_name]
                    # print(pc._dstport )
        pc._protocol = str(packet.layers[n - 1].layer_name)
        pc._protocol_transport = packet.layers[n - 1].layer_name
        return pc
    if n == 2:
        layerip = packet.layers[n-1]
        for field_name in layerip.field_names:
            if len(field_name) > 0:
                if field_name == 'src_host':
                    layer_fields1[field_name] = getattr(layerip, field_name)
                    pc._src_host = layer_fields1[field_name]
                    # print(pc._src_host )
                if field_name == 'dst_host':
                    layer_fields1[field_name] = getattr(layerip, field_name)
                    pc._dst_host = layer_fields1[field_name]
                    # print(pc._dst_host )
        pc._protocol = packet.layers[n-1].layer_name
        pc._protocol_transport = packet.layers[n - 1].layer_name
        return pc
    if n >=4:
        layerip = packet.layers[1]
        layertcp = packet.layers[2]
        for field_name in layerip.field_names:
            if len(field_name) > 0:
                if field_name == 'src_host':
                    layer_fields1[field_name] = getattr(layerip, field_name)
                    pc._src_host = layer_fields1[field_name]
                    # print(pc._src_host )
                if field_name == 'dst_host':
                    layer_fields1[field_name] = getattr(layerip, field_name)
                    pc._dst_host = layer_fields1[field_name]
                    # print(pc._dst_host )
        for field_name in layertcp.field_names:
            if len(field_name) > 0:
                if field_name == 'srcport':
                    layer_fields2[field_name] = getattr(layertcp, field_name)
                    pc._srcport = layer_fields2[field_name]
                    # print(pc._srcport )
                if field_name == 'dstport':
                    layer_fields2[field_name] = getattr(layertcp, field_name)
                    pc._dstport = layer_fields2[field_name]
                    # print(pc._dstport )
        pc._protocol = str(packet.layers[n-1].layer_name)
        pc._protocol_transport = packet.layers[n - 1].layer_name
        return pc
# Index packets in Elasticsearch
def index_packets(capture, pcap_file, file_date_utc, count):
    pkt_no = 1
    for packet in capture:
        highest_protocol, layers = get_layers(packet)
        sniff_timestamp = float(packet.sniff_timestamp) # use this field for ordering the packets in ES
        if pcap_file == None:
            action = {
                '_op_type' : 'index',
                '_index' : 'packets-'+datetime.utcfromtimestamp(sniff_timestamp).strftime('%Y-%m-%d'),
                '_type' : 'pcap_live',
                '_source' : {
                    'sniff_date_utc' : datetime.utcfromtimestamp(sniff_timestamp).strftime('%Y-%m-%dT%H:%M:%S+0000'),
                    'sniff_timestamp' : sniff_timestamp,
                    'protocol' : highest_protocol,
                    'layers' : layers
                }
            }
            yield action
        else:
            action = {
                '_op_type' : 'index',
                '_index' : 'packets-'+datetime.utcfromtimestamp(sniff_timestamp).strftime('%Y-%m-%d'),
                '_type' : 'pcap_file',
                '_source' : {
                    'file_name' : pcap_file,
                    'file_date_utc' : file_date_utc.strftime('%Y-%m-%dT%H:%M:%S'),
                    'sniff_date_utc' : datetime.utcfromtimestamp(sniff_timestamp).strftime('%Y-%m-%dT%H:%M:%S+0000'),
                    'sniff_timestamp' : sniff_timestamp,
                    'protocol' : highest_protocol,
                    'layers' : layers
                }
            }
            yield action
        pkt_no += 1
        if count > 0 and pkt_no > count:
            return

# Dump raw packets to stdout
def dump_packets(capture, file_date_utc,feature_extraction,model):
    pkt_no = 1
    pet_list = []
    for packet in capture:
        # highest_protocol, layers = get_layers(packet)
        print ('Packet no.', pkt_no)
        pc = get_packet(packet)

        d = {'index':[pkt_no], 'method':[pc._method], 'url':[pc._url], 'protocol':[pc._protocol], 'userAgent':[pc._useragent], 'pragma':[pc._pragma],
       'cacheControl':[pc._cachecontrol], 'accept':[pc._accept], 'acceptEncoding':[pc._acceptencoding],
       'acceptCharset':[pc._acceptcharset],
       'acceptLanguage':[pc._acceptlanguage], 'host':[pc._host], 'connection':[pc._connection], 'contentLength':[pc._contentlengt], 'contentType':[pc._contenttype],
       'cookie':['null'], 'payload':['null']}
        # print('index',[pkt_no], 'method',[pc._method], 'url',[pc._url], 'protocol',[pc._protocol], 'userAgent',[pc._useragent], 'pragma',[pc._pragma],
        #    'cacheControl',[pc._cachecontrol], 'accept',[pc._accept], 'acceptEncoding',[pc._acceptencoding],
        #    'acceptCharset',[pc._acceptcharset],
        #    'acceptLanguage',[pc._acceptlanguage], 'host',[pc._host], 'connection',[pc._connection], 'contentLength',[pc._contentlengt], 'contentType',[pc._contenttype],
        #    'cookie',['null'], 'payload',['null'])
        dp = pd.DataFrame(d)
        x_testing = feature_extraction.transform(dp)
        y_testing = model.predict(x_testing)
        print(y_testing)
        pkt_no += 1
        pet_list.clear()
        pet_list.append(pc._src_host)
        pet_list.append(pc._dst_host)
        pet_list.append(pc._srcport)
        pet_list.append(pc._dstport)
        pet_list.append(pc._protocol)
        print(pet_list)
    return pet_list
# Live capture function
def live_capture(nic,filter,feature_extraction,model):
    print(nic)
    sniff_date_utc = datetime.utcnow()
    capture = pyshark.LiveCapture(interface=nic,display_filter=filter)
    dump_packets(capture, sniff_date_utc,feature_extraction,model)

    # return list
def file_capture(pcap_files, node, chunk):
    try:
        es = None
        if node != None:
            es = Elasticsearch(node)

        print ('Loading packet capture file(s)')
        for pcap_file in pcap_files:
            print (pcap_file)
            stats = os.stat(pcap_file)
            file_date_utc = datetime.utcfromtimestamp(stats.st_ctime)
            capture = pyshark.FileCapture(pcap_file)

            # If no Elasticsearch node specified, dump to stdout
            if node == None:
                dump_packets(capture, file_date_utc, 0)
            else:
                helpers.bulk(es, index_packets(capture, pcap_file, file_date_utc, 0), chunk_size=chunk, raise_on_error=True)
    except Exception as e:
        print ('[ERROR] ', e)
