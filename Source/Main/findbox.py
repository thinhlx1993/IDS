# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'find_box.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QWidget
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Find(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setObjectName(_fromUtf8("Find"))
        self.resize(452, 109)
        self.widget = QtGui.QWidget(self)
        self.widget.setGeometry(QtCore.QRect(0, 0, 451, 111))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.gridLayout = QtGui.QGridLayout(self.widget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(self.widget)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtGui.QLineEdit(self.widget)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout.addWidget(self.lineEdit, 0, 1, 1, 1)
        self.but_find = QtGui.QPushButton(self.widget)
        self.but_find.setObjectName(_fromUtf8("but_find"))
        self.gridLayout.addWidget(self.but_find, 0, 2, 1, 1)
        self.but_cancel = QtGui.QPushButton(self.widget)
        self.but_cancel.setObjectName(_fromUtf8("but_cancel"))
        self.but_cancel.clicked.connect(self.but_cancel_push)
        self.gridLayout.addWidget(self.but_cancel, 1, 2, 1, 1)

        self.retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self, Find):
        Find.setWindowTitle(_translate("Find", "Find", None))
        self.label.setText(_translate("Find", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt;\">Find What</span></p></body></html>", None))
        self.but_find.setText(_translate("Find", "Find", None))
        self.but_cancel.setText(_translate("Find", "Cancel", None))

    def but_cancel_push(self):
        self.close()