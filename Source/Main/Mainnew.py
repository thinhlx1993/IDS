from datetime import datetime
import pandas as pd
from PyQt4 import QtCore, QtGui
from Capture import Capture
from Main import interface as box_capture
from Main import endpoint as box_endpoint
from Main import findbox as find_box
from Machine.FeatureExtactor import TextExtractor
import joblib
import re
import netifaces as ni
import tkFileDialog
import Tkinter
import socket
from sklearn.ensemble import VotingClassifier
import joblib
import numpy as np
sniff_date_utc = datetime.utcnow()
feature_extraction = joblib.load('//home/thinh/Documents/feature_union.joblib')
model = joblib.load('//home/thinh/Documents/model_randomforest.joblib')
# voting_class =  joblib.load('/home/thinh/Documents/voting/model_voting.joblib')
# ex = joblib.load('/home/thinh/Documents/voting/feature_union.joblib')
import pyshark
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys #import
global list_des_host
list_des_host = []
global list_ipv4
list_ipv4 = []
global count_ipv4
count_ipv4 = []
global count_ipv6
count_ipv6 = []
ipv4_ip = re.compile('.*.*.*.*')
global list_ipv6
list_ipv6  = []
ipv6_ip = re.compile('.*::*')
global list_tcp
list_tcp = []
global list_udp
list_udp  = []
global list_http
list_http = []
global http_packet
http_packet = 0
global listoflist
listoflist = []#varian
global pkt_no
global packet
global pc
global length_packet
length_packet = []
global count_length_packet
count_length_packet = []
global c_stop
global access_name


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class MainWindow(QMainWindow):
    def __init__(self, *args):
        QMainWindow.__init__(self, *args)
        self.setObjectName(_fromUtf8("MainWindow"))
        self.resize(1200, 900)
        self.centralwidget = QtGui.QWidget(self)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.table = QtGui.QTableWidget(self.centralwidget)
        self.table.setGeometry(QtCore.QRect(0, 50, 1191, 601))
        self.table.setObjectName(_fromUtf8("listView"))
        self.table.setRowCount(0)
        self.table.setColumnCount(7)
        self.table.setHorizontalHeaderLabels(['Time', 'Source IP', 'Destination IP','Protocol', 'Source Port', 'Destination Port','Label'])
        self.table.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(5, 5, 700, 50))
        self.label.setObjectName(_fromUtf8("label"))
        self.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(self.centralWidget())
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1200, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuEdit = QtGui.QMenu(self.menubar)
        self.menuEdit.setObjectName(_fromUtf8("menuEdit"))
        self.menuView = QtGui.QMenu(self.menubar)
        self.menuView.setObjectName(_fromUtf8("menuView"))
        self.menuCapture = QtGui.QMenu(self.menubar)
        self.menuCapture.setObjectName(_fromUtf8("menuCapture"))
        self.menuCapture_2 = QtGui.QMenu(self.menuCapture)
        self.menuCapture_2.setObjectName(_fromUtf8("menuCapture_2"))
        self.menuStatics = QtGui.QMenu(self.menubar)
        self.menuStatics.setObjectName(_fromUtf8("menuStatics"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        self.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(self)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        self.setStatusBar(self.statusbar)
        self.actionOpen = QtGui.QAction(self)
        self.actionOpen.setObjectName(_fromUtf8("actionOpen"))
        self.actionOpen.triggered.connect(self.start_file_capture)
        self.actionClose = QtGui.QAction(self)
        self.actionClose.setObjectName(_fromUtf8("actionClose"))
        self.actionClose.triggered.connect(self.clear_action)
        self.actionSave = QtGui.QAction(self)
        self.actionSave.setObjectName(_fromUtf8("actionSave"))
        self.actionSaveas = QtGui.QAction(self)
        self.actionSaveas.setObjectName(_fromUtf8("actionSaveas"))
        self.action_exit = QtGui.QAction(self)
        self.action_exit.setObjectName(_fromUtf8("action_exit"))
        self.action_exit.triggered.connect(self.exit_action)
        self.actionFind = QtGui.QAction(self)
        self.actionFind.setObjectName(_fromUtf8("actionFind"))
        self.actionFind.triggered.connect(self.show_find_box)
        self.actionFind_next = QtGui.QAction(self)
        self.actionFind_next.setObjectName(_fromUtf8("actionFind_next"))
        self.actionOption = QtGui.QAction(self)
        self.actionOption.setObjectName(_fromUtf8("actionOption"))
        self.actionCapture_Filter = QtGui.QAction(self)
        self.actionCapture_Filter.setObjectName(_fromUtf8("actionCapture_Filter"))
        self.actionStart = QtGui.QAction(self)
        self.actionStart.setObjectName(_fromUtf8("actionStart"))
        self.actionStart.triggered.connect(self.start)
        self.actionStop = QtGui.QAction(self)
        self.actionStop.setObjectName(_fromUtf8("actionStop"))
        self.actionStop.triggered.connect(self.stop_capture)
        self.actionPause = QtGui.QAction(self)
        self.actionPause.setObjectName(_fromUtf8("actionPause"))
        self.actionClear = QtGui.QAction(self)
        self.actionClear.setObjectName(_fromUtf8("actionClear"))
        self.actionClear.triggered.connect(self.clear_action)
        self.actionRefresh_Interface = QtGui.QAction(self)
        self.actionRefresh_Interface.setObjectName(_fromUtf8("actionRefresh_Interface"))
        self.actionEnd_Points = QtGui.QAction(self)
        self.actionEnd_Points.setObjectName(_fromUtf8("actionEnd_Points"))
        self.actionCapture_Graps = QtGui.QAction(self)
        self.actionCapture_Graps.setObjectName(_fromUtf8("actionCapture_Graps"))
        self.actionEnd_Point = QtGui.QAction(self)
        self.actionEnd_Point.setObjectName(_fromUtf8("actionEnd_Point"))
        self.actionEnd_Point.triggered.connect(self.endpoint)
        self.actionWeb_access = QtGui.QAction(self)
        self.actionWeb_access.setObjectName(_fromUtf8("actionWeb_access"))
        self.actionWeb_access.triggered.connect(self.access_list_show)
        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addAction(self.actionClose)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.action_exit)
        self.menuEdit.addAction(self.actionFind)
        self.menuView.addAction(self.actionClear)
        self.menuView.addAction(self.actionRefresh_Interface)
        self.menuCapture.addAction(self.actionStart)
        self.menuCapture.addAction(self.actionStop)
        self.menuCapture.addSeparator()
        self.menuStatics.addAction(self.actionEnd_Point)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuView.menuAction())
        self.menubar.addAction(self.menuCapture.menuAction())
        self.menubar.addAction(self.menuStatics.menuAction())
        self.menubar.setNativeMenuBar(False)
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)


    def retranslateUi(self):
        self.setWindowTitle(_translate("IDS Program", "IDS Program", None))
        self.label.setText(_translate("MainWindow",
                                      "<html><head/><body><p align=\"center\"><span style=\" font-size:22pt;\">IDS Program, Le Xuan Thinh, Cao Minh Ngoc</span></p><p align=\"center\"><br/></p></body></html>",
                                      None))
        self.menuFile.setTitle(_translate("MainWindow", "File", None))
        self.menuEdit.setTitle(_translate("MainWindow", "Edit", None))
        self.menuView.setTitle(_translate("MainWindow", "View", None))
        self.menuCapture.setTitle(_translate("MainWindow", "Capture", None))
        self.menuCapture_2.setTitle(_translate("MainWindow", "Capture", None))
        self.menuStatics.setTitle(_translate("MainWindow", "Satatic", None))
        self.menuHelp.setTitle(_translate("MainWindow", "Help", None))
        self.actionOpen.setText(_translate("MainWindow", "Open", None))
        self.actionClose.setText(_translate("MainWindow", "Close", None))
        self.actionSave.setText(_translate("MainWindow", "Save", None))
        self.actionSaveas.setText(_translate("MainWindow", "Save as", None))
        self.action_exit.setText(_translate("MainWindow", "Exit", None))
        self.actionFind.setText(_translate("MainWindow", "Find", None))
        self.actionFind_next.setText(_translate("MainWindow", "Find Next", None))
        self.actionOption.setText(_translate("MainWindow", "Option", None))
        self.actionCapture_Filter.setText(_translate("MainWindow", "Capture Filter", None))
        self.actionStart.setText(_translate("MainWindow", "Start", None))
        self.actionStop.setText(_translate("MainWindow", "Stop", None))
        self.actionPause.setText(_translate("MainWindow", "Pause", None))
        self.actionClear.setText(_translate("MainWindow", "Clear", None))
        self.actionRefresh_Interface.setText(_translate("MainWindow", "Refresh Interface", None))
        self.actionEnd_Points.setText(_translate("MainWindow", "End Points", None))
        self.actionCapture_Graps.setText(_translate("MainWindow", "Capture Graps", None))
        self.actionEnd_Point.setText(_translate("MainWindow", "End Point", None))
        self.actionWeb_access.setText(_translate("MainWindow", "Web Access", None))
    def doit(self):
        print ('end')
    def exit_action(self):
        self.destroy()
    def access_list_show(self):
        return

    def show_find_box(self):
        self.ui = find_box.Ui_Find()
        self.ui.but_find.clicked.connect(self.find_items)
        self.ui.show()
    def find_items(self):
        text_find  = self.ui.lineEdit.text()
        row = self.table.rowCount()
        for items_row in range(0,row):
            for items_collum in range(0,7):
                item = self.table.item(row - 1, items_collum)
                text = item.text()
                if text_find == text:
                    self.table.item(items_row, items_collum).setBackgroundColor(QtGui.QColor(235, 243, 22))
    def flatten(self,seq, container=None):
        if container is None:
            container = []
        for s in seq:
            if hasattr(s, '__iter__'):
                self.flatten(s, container)
            else:
                container.append(s)
        return container
    def endpoint(self):
        self.e = box_endpoint.Ui_EndPoint()
        a=0
        b=0
        c = self.flatten(count_ipv4)
        d = self.flatten((count_ipv6))
        n = len (list_ipv4)
        k = len (list_ipv6)
        f = len(count_ipv4)

        for tempc in range(0,n):
            count_length_packet.append(tempc)
        # print  len(count_length_packet)
        for tempa in range(0,n):
            count_length_packet[tempa] = 0
            for tempb in range(0,f):
                if list_ipv4[tempa] == count_ipv4[tempb]:
                    count_length_packet[tempa] += int(length_packet[tempb])
        for i in range(0,n):
            temp = c.count(list_ipv4[i])
            name, alias, addresslist = self.lookup(list_ipv4[i])
            print name,alias,addresslist
            rowPosition = self.e.ipv4_tablewiget.rowCount()
            self.e.ipv4_tablewiget.insertRow(rowPosition)
            self.e.ipv4_tablewiget.setItem(a, 0, QTableWidgetItem(list_ipv4[i]))
            self.e.ipv4_tablewiget.setItem(a, 1, QTableWidgetItem(str(name)))
            self.e.ipv4_tablewiget.setItem(a, 2, QTableWidgetItem(str(temp)))
            self.e.ipv4_tablewiget.setItem(a, 3,QTableWidgetItem(str(count_length_packet[i])))
            a += 1
        for i in range(0, k):
            temp = d.count(list_ipv6[i])
            rowPosition = self.e.ipv6_tablewiget.rowCount()
            self.e.ipv6_tablewiget.insertRow(rowPosition)
            self.e.ipv6_tablewiget.setItem(b, 0, QtGui.QTableWidgetItem(list_ipv6[i]))
            self.e.ipv6_tablewiget.setItem(b, 1, QtGui.QTableWidgetItem(str(temp)))
            b+=1
        self.e.show()

    def lookup(self,addr):
        try:
            return socket.gethostbyaddr(addr)
        except socket.herror:
            return None, None, None
    def refreshCallback(self):
        self.centralWidget().show()
        QtCore.QCoreApplication.processEvents()
    def clear_table(self):
        self.table.clear()
        self.table.setRowCount(0)
        self.table.setColumnCount(7)
        self.table.setHorizontalHeaderLabels(['Time', 'Source IP', 'Destination IP', 'Protocol', 'Source Port', 'Destination Port', 'Label'])
    def timers(self):
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(1000)
        self.box_capture_show = box_capture.Ui_Form()
        self.box_capture_show.show()
        self.box_capture_show.pushButton.clicked.connect(self.get_interface)
    def get_interface(self):
        interface = self.box_capture_show.comboBox.currentText()
        self.box_capture_show.close()
        self.live_capture(interface)
    def file_capture_timers(self):
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(1000)
        self.connect(self.file_capture(), SIGNAL("clicked()"), self.doit)
    def start(self):
        self.timers()
        self.timer.start()
    def start_file_capture(self):
        self.file_capture_timers()
        self.start()
    def stop_capture(self):
        self.c_stop = 1
    def clear_action(self):
        self.stop_capture()
        self.table.clear()
        self.table.setRowCount(0)
        self.table.setColumnCount(7)
        self.table.setHorizontalHeaderLabels(['Time', 'Source IP', 'Destination IP', 'Protocol', 'Source Port', 'Destination Port', 'Label'])
    def file_capture(self):

        root = Tkinter.Tk()
        root.withdraw()
        filename = tkFileDialog.askopenfile()

        self.clear_table()
        sniff_date_utc = datetime.utcnow()
        length_packet = []
        pkt_no = 0
        self.c_stop = 0
        # print str(interface)
        capture = pyshark.FileCapture(filename)  # bat goi tin
        for packet in capture:
            if self.c_stop == 1:
                return
            pc = Capture.get_packet(packet)
            # if pc._protocol_transport == 'http':
            self.show_form(pc, pkt_no, packet.length)
            pkt_no += 1
            self.refreshCallback()
    def live_capture(self,interface):

        self.clear_table()
        sniff_date_utc = datetime.utcnow()
        length_packet = []
        pkt_no = 0
        self.c_stop = 0
        # print str(interface)
        capture = pyshark.LiveCapture(interface=str(interface))  # bat goi tin
        for packet in capture.sniff_continuously(packet_count=0):
            if self.c_stop == 1:
                return
            pc = Capture.get_packet(packet)
            if pc._protocol_transport == 'http':
                self.show_form(pc,pkt_no,packet.length)

            # print('index', [pkt_no], 'method', [pc._method], 'url', [pc._url], 'protocol', [pc._protocol], 'userAgent',
            #     [pc._useragent], 'pragma', [pc._pragma],
            #     'cacheControl', [pc._cachecontrol], 'accept', [pc._accept], 'acceptEncoding', [pc._acceptencoding],
            #     'acceptCharset', [pc._acceptcharset],
            #     'acceptLanguage', [pc._acceptlanguage], 'host', [pc._host], 'connection', [pc._connection],
            #     'contentLength', [pc._contentlengt], 'contentType', [pc._contenttype])

                pkt_no+=1
            self.refreshCallback()
    def show_form(self,pc,pkt_no,pc_length):

        rowPosition = self.table.rowCount()
        self.table.insertRow(rowPosition)

        y_pred = model.predict(feature_extraction.transform(pd.DataFrame({'method': [pc._method], 'protocol': [pc._protocol], 'userAgent': [pc._useragent],
                           'pragma': [pc._pragma],
                           'cacheControl': [pc._cachecontrol], 'accept': [pc._accept],
                           'acceptEncoding': [pc._acceptencoding],
                           'acceptCharset': [pc._acceptcharset],
                           'acceptLanguage': [pc._acceptlanguage], 'host': [pc._host], 'connection': [pc._connection],
                           'contentLength': [pc._contentlengt], 'contentType': [pc._contenttype]})))
        # print y_pred
        if ipv4_ip.match(pc._dst_host) is not None:
            if pc._dst_host not in list_ipv4:
                list_ipv4.append(pc._dst_host)
                # print 'ipv4: ', list_ipv4
            count_ipv4.append(pc._dst_host)
            length_packet.append(pc_length)

        if ipv6_ip.match(pc._src_host) is not None:
            if pc._src_host not in list_ipv6:
                list_ipv6.append(pc._dst_host)
                # print 'ipv6: ', list_ipv6
            count_ipv6.append(pc._dst_host)

        self.table.setItem(pkt_no, 0, QtGui.QTableWidgetItem(sniff_date_utc.ctime()))  # show
        self.table.setItem(pkt_no, 1, QtGui.QTableWidgetItem(pc._src_host))  # show
        self.table.setItem(pkt_no, 2, QtGui.QTableWidgetItem(pc._dst_host))  # show
        self.table.setItem(pkt_no, 3, QtGui.QTableWidgetItem(pc._protocol))  # show
        self.table.setItem(pkt_no, 4, QtGui.QTableWidgetItem(pc._dstport))  # show
        self.table.setItem(pkt_no, 5, QtGui.QTableWidgetItem(pc._srcport))  # show
        self.table.setItem(pkt_no, 6, QtGui.QTableWidgetItem(str(y_pred)))  # show
        if y_pred == ['anom']:
            self.table.item(pkt_no, 0).setBackground(QtGui.QColor(255, 0, 0))
            self.table.item(pkt_no, 1).setBackground(QtGui.QColor(255, 0, 0))
            self.table.item(pkt_no, 2).setBackground(QtGui.QColor(255, 0, 0))
            self.table.item(pkt_no, 3).setBackground(QtGui.QColor(255, 0, 0))
            self.table.item(pkt_no, 4).setBackground(QtGui.QColor(255, 0, 0))
            self.table.item(pkt_no, 5).setBackground(QtGui.QColor(255, 0, 0))
            self.table.item(pkt_no, 6).setBackground(QtGui.QColor(255, 0, 0))

        self.refreshCallback()
class App(QApplication):
    def __init__(self, *args):
        QApplication.__init__(self, *args)
        self.main = MainWindow()
        self.connect(self, SIGNAL("Closed()"), self.byebye )
        self.main.show()

    def byebye( self ):
        self.exit(0)
def main(args):
    global app
    app = App(args)
    app.exec_()

if __name__ == "__main__":
    main(sys.argv)