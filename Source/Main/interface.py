# -*- coding: utf-8 -*-
#
# Form implementation generated from reading ui file 'interface.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import netifaces as ni
global iface
import sys
from PyQt4.Qt import *
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setObjectName(_fromUtf8("Form"))
        self.resize(291, 67)
        self.setGeometry(QtCore.QRect(291, 67, 291, 67))
        self.pushButton = QtGui.QPushButton(self)
        self.pushButton.setGeometry(QtCore.QRect(190, 20, 98, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.but_pusht)
        self.comboBox = QtGui.QComboBox(self)
        self.comboBox.setGeometry(QtCore.QRect(0, 20, 181, 27))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        for interface in ni.interfaces():
            self.comboBox.addItem(interface)
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        self.setWindowTitle(_translate("Form", "Select Interface", None))
        self.pushButton.setText(_translate("Form", "Capture", None))
    def but_pusht(self):
        return


