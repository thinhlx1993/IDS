# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'access.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QWidget
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_Form(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setObjectName(_fromUtf8("AccessList"))
        self.resize(444, 207)
        self.access_list = QtGui.QTableWidget(self)
        self.access_list.setGeometry(QtCore.QRect(0, 0, 451, 211))
        self.access_list.setObjectName(_fromUtf8("access_list"))
        self.access_list.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.access_list.setColumnCount(0)
        self.access_list.setRowCount(0)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        self.setWindowTitle(_translate("AccessList", "Access_list", None))

