# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'endpoint.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QWidget
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_EndPoint(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setObjectName(_fromUtf8("EndPoint"))
        self.resize(666, 284)
        self.endpoint = QtGui.QTabWidget(self)
        self.endpoint.setGeometry(QtCore.QRect(0, 0, 661, 281))
        self.endpoint.setObjectName(_fromUtf8("endpoint"))
        self.IpV4 = QtGui.QWidget()
        self.IpV4.setObjectName(_fromUtf8("IpV4"))
        self.ipv4_tablewiget = QtGui.QTableWidget(self.IpV4)
        self.ipv4_tablewiget.setGeometry(QtCore.QRect(0, 0, 661, 251))
        self.ipv4_tablewiget.setObjectName(_fromUtf8("ipv4_tablewiget"))
        self.ipv4_tablewiget.setRowCount(0)
        self.ipv4_tablewiget.setColumnCount(4)
        self.ipv4_tablewiget.setHorizontalHeaderLabels(['Address', 'Host Name','Packet', 'Byte Transfer'])
        self.ipv4_tablewiget.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.endpoint.addTab(self.IpV4, _fromUtf8(""))
        self.ipv6 = QtGui.QWidget()
        self.ipv6.setObjectName(_fromUtf8("ipv6"))
        self.ipv6_tablewiget = QtGui.QTableWidget(self.ipv6)
        self.ipv6_tablewiget.setGeometry(QtCore.QRect(0, 0, 661, 251))
        self.ipv6_tablewiget.setObjectName(_fromUtf8("ipv6_tablewiget"))
        self.ipv6_tablewiget.setRowCount(0)
        self.ipv6_tablewiget.setColumnCount(3)
        self.ipv6_tablewiget.setHorizontalHeaderLabels(['Address', 'Packet', 'Byte Transfer'])

        self.retranslateUi()
        self.endpoint.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        self.setWindowTitle(_translate("EndPoint", "EndPoint", None))
        self.endpoint.setTabText(self.endpoint.indexOf(self.IpV4), _translate("EndPoint", "IpV4", None))
        self.endpoint.setTabText(self.endpoint.indexOf(self.ipv6), _translate("EndPoint", "IpV6", None))


